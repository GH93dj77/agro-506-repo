
document.write(`
<div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>Dashboard</h2>
                    <ul class="breadcrumb p-l-0 p-b-0">
                        <li class="breadcrumb-item"><a href="../home/dashboard.html"><i class="zmdi zmdi-home"></i></a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ul>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12">
                    <div class="input-group m-b-0">                
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-addon">
                            <i class="zmdi zmdi-search"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
`);
var globalUser;
window.onload = function () {
    $("page-loader-wrapper").fadeIn(500);
    // var dummyUser = {
    //     id: "01010",
    //     nombre: "DevUser",
    //     correo: "devUser@gmail.com"
    // }
    // setPage(dummyUser);
    
    ChatApp.globalGetUser(function (rest) {
        let tipoUsuario = rest.tipo == 0?"Administrador":"Usuario";
        $("#sidebar-nameuser").text(rest.nombre+" "+rest.apellido);
        $("#sidebar-tipousuario").text(tipoUsuario);
        console.log("rest header usuario => " + JSON.stringify(rest));
        console.log("USUARIO");
        globalUser = rest;
        if(rest.tipo != 1){
            window.location.href = "../../page/admin/Dashboard/dashboard.html";
        }

        // setPage() establece caracteristicas especificade esa pagina hacia el usuario actual.
        //[@param:JSON]
        setPage(rest);
        // $('.content').fadeIn(150);
        // $('.page-sidebar').fadeIn(150);
        // $('.footer').fadeIn(150);

    });


};