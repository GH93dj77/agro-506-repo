/*
Titulo:  log-in.js
Descripción: Gestiona funciones para login.html

Datos del autor : 
-Asdrubal Torres Siles

Version 1.0
Ultima modificacion : 21/11/2021

Otros cambios:
Sin cambios nuevos
*/

var ChatApp = window.ChatApp || {};


/**
 * @var CognitoGlobal referencia global a la instancia del API de cognito
 */
var CognitoGlobal; 

/**
 * @var userNameGlobal acceso global al nombre del usu
 */
var userNameGlobal;

/**
 * @var colaborador_codigo acceso global al ID del usuario
 */
var colaborador_codigo;

/**
 * @var sessionUserAttributesGlobal acceso global a los atributos de sesion
 */
var sessionUserAttributesGlobal;


/**
 * @var newPassObject objeto cognito conteniendo la contraseña del usuario
 */
var newPassObject;

 

var indexDirAdmin =  "../../page/admin/Dashboard/dashboard.html"; 
var indexDirUser =  "../../page/home/dashboard.html"; 
var loginDir = "../../page/log-in/log-in.html";

var userneameInputID =  "#username"; 
var passwordInputID = "#password";


(function scopeWrapper($) {  
    $(".page-loader-wrapper").fadeIn(3000);
    var apiClient = apigClientFactory.newClient();  // Instancia del API de aws
    /**
     * @var userPool instancia al pool de usuarios
     */
    var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
    /**
     * @function checkLogin Confirma si el usuario ya inicio sesion
     * @param redirectOnRec usuario reconocido , se redirecionara al index.html
     * @param redirectOnUnrec usuario no reconocido , se redirecionara al login.html
     */


    ChatApp.checkLogin = function (redirectOnRec, redirectOnUnrec) {
        
        var cognitoUser = userPool.getCurrentUser();
        if (cognitoUser !== null) {
            $(".page-loader-wrapper").fadeIn(800);
            let idUsuario = cognitoUser.username;
            ChatApp.globalGetUser(idUsuario, function (rest) {
                $(".page-loader-wrapper").fadeOut(800);
                console.log("rest chekc = " + JSON.stringify(rest));
                if(rest.tipo == 0){
                    window.location = indexDirAdmin
                }else{
                    window.location = indexDirUser
                }
             });   
                
        
        } else {
            //document.getElementById("second_load_screen").style.display = "none";
            console.log("Sin sesion");
        }
    };

    /**
     * @function login Realiza log-in con los elementos que contienen @userneameInputID y @passwordInputID . 
     * Tambien reliza verificaciones si; el usuario no esta verificado, el correo o contraseña es incorrecta o la cantida de intentos se exedio
     */
    ChatApp.login = function (e) {
        //e.preventDefault();
        var username = $('#txtCorreo').val();
        var contrasenia = $('#txtContrasenia').val();
        var authenticationData = {
            Username: username,
            Password: contrasenia
        };
        if (!username && !contrasenia) {
            alert("Complete la información.");
            return;

        }

        userNameGlobal = username;

        var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
        var userData = {
            Username: username,
            Pool: userPool
        };
        var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

        CognitoGlobal = cognitoUser;
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (data) {
                if (data.accessToken.payload.username) {

                    let idUsuario = data.accessToken.payload.username;

                    ChatApp.globalGetUser(idUsuario, function (rest) {

                        if(rest.tipo == 0){
                            window.location = indexDirAdmin
                        }else{
                            window.location = indexDirUser
                        }
                    });       
                }
                else {
                    alert("Hubo un error inesperado, por favor verifique las credenciales!!");
                }
            },
            onFailure: function (err) {
                console.log(err);
                if (err.code == "UserNotConfirmedException") {
                    alert("Usuario no verificado");
                    console.log(err);
                    document.getElementById("second_load_screen").style.display = "none";
                    return;
                } else if (err.code == "UsernameExistsException") {
                    alert("Correo o contraseña invalida");
                    console.log(err);
                    document.getElementById("second_load_screen").style.display = "none";
                    return;
                } else if (err.code == "LimitExceededException") {
                    alert("Numero de intentos superado, intente dentro de unos minutos");
                    document.getElementById("second_load_screen").style.display = "none";
                }
                else if (err.code == "InvalidPasswordException") {
                    alert("La nueva contraseña no satisface lo requisitos, esta tiene que ser mayor de 8 caracteres, y contener al menos un numero y una letra mayúscula");
                    document.getElementById("second_load_screen").style.display = "none";
                }
                else if (err.message == "Incorrect username or password.") {
                    console.log(err.message);
                    alertify.alert('Error', 'Las credenciales ingresadas no son correctas.!');
                    document.getElementById("second_load_screen").style.display = "none";
                }

                document.getElementById("second_load_screen").style.display = "none";
            },
            newPasswordRequired: function (userAttributes, requiredAttributes) {
                $("#loader-overlay").fadeOut(800);
                // User was signed up by an admin and must provide new
                // password and required attributes, if any, to complete
                // authentication.

                // the api doesn't accept this field back
                delete userAttributes.email_verified;

                // store userAttributes on global variable
                // sessionUserAttributes = userAttributes;
                sessionUserAttributesGlobal = userAttributes;
                $("#formIngreso").fadeOut("fast", function () {
                    $("#formNewPass").fadeIn("slow");
                });
            }
        });
    };

    ChatApp.handleNewPassword = function() {
    $("#loader-overlay").fadeIn(500);
        var newPass = $("#newPassword").val();
        CognitoGlobal.completeNewPasswordChallenge(
            newPass,
            sessionUserAttributesGlobal,
            {
            onSuccess: function (result) {
                // User authentication was successful
                userNameGlobal = result.accessToken.payload.username;
                ChatApp.globalGetUser(userNameGlobal, function (rest) {

                    if(rest.tipo == 0){
                        window.location = indexDirAdmin
                    }else{
                        window.location = indexDirUser
                    }
                });   
            },
            onFailure: function (err) {
                // User authentication was not successful
                console.log(err);
                alert("Error al actualizar la contraseña");
                $("#loader-overlay").fadeOut(800);
            },
            }
        );
    }



    ChatApp.globalGetUser = function (id, callback) {

        let params = {
            id: id
        };
        try {
            useToken(token, function (token) {
                apiClient
                .usuarioGetItemPost(null, params, {
                    headers: {
                    Authorization: token,
                    },
                })
                .then(function (result) {
                    if (result.data.hasOwnProperty("errorMessage")) {
                        alert("Error: " + result.data.errorMessage);
                        // document.getElementById("loader-overlay-MOD").style.display =
                        //     "none";
                        window.location.href = "log-in.html";
                        return;
                    // location.reload();
                    }
                    if (result.status == 200) {
                        callback(result.data);
                    } else {
                        // document.getElementById("loader-overlay-MOD").style.display =
                        //     "none";
                        alert("Ocurrió un error al guardar los datos.");
                        return;
                    }
                });
            });
            } catch (e) {
            console.log("error :" + e);
            document.getElementById("loader-overlay-MOD").style.display = "none";
            alert("Ocurrió un error con la conexión.");
            } 
    }

}(jQuery));

$(window).on("load", function () {
    $("body").fadeIn(1000, function () {
       $(".page-loader-wrapper").fadeOut(500);
    });
});

