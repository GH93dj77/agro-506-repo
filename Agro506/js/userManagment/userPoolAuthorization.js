// var token = null;
// var userPoolAuthorization = new AmazonCognitoIdentity.CognitoUserPool(poolData);

// $(document).ready(function() {

//     var cognitoUser = userPoolAuthorization.getCurrentUser();
//     if (cognitoUser == null) {

//         window.location = '../../page/log-in/log-in.html';
            
//     }
// });

// function useToken(token, callback) {
//     if (token === null) {
//         var cognitoUser = userPoolAuthorization.getCurrentUser();
//         if (cognitoUser !== null) {
//             cognitoUser.getSession(function (err, session) {
//                 if (err) {
//                     window.location = '../../page/log-in/log-in.html';
//                 }
//                 token = session.getIdToken().getJwtToken();
//                 callback(token);
//             });
//         }
//     } else {
//         callback(token);
//     }
// };

var token = null;
var userPoolAuthorization = new AmazonCognitoIdentity.CognitoUserPool(poolData);

var indexDir = "../../page/admin/dashboard/dashboard.html";
var loginDir = "../../page/log-in/log-in.html";

function useToken(token, callback) {
    if (token === null) {
        var cognitoUser = userPoolAuthorization.getCurrentUser();
        if (cognitoUser !== null) {
            cognitoUser.getSession(function (err, session) {
                if (err) {
                    window.location = loginDir;
                }
                token = session.getIdToken().getJwtToken();
                callback(token);
            });
        }
    } else {
        callback(token);
    }
};

async function useTokenV2() {
    var token = "";
  
    // 1) Create the poolData object
    // const poolData = {
    //   UserPoolId: poolData.UserPoolId,
    //   ClientId: poolData.ClientId
    // };
    // 2) Initialize the userPool interface
    var userPool = new window.AmazonCognitoIdentity.CognitoUserPool(poolData);
  
    var cognitoUser = userPool.getCurrentUser();
    if (cognitoUser !== null) {
      cognitoUser.getSession((err, session) => {
        if (err === null) {
          token = session.getIdToken().getJwtToken();
        }
      });
    }
  
    return token;
  }