/*
Titulo:  customDropzone.js
Descripción: Gestiona funciones para agregar/editar-contrato.html

Datos del autor : 
-Asdrubal Torres Siles
-Empresa: TECHBITE

Version 1.0
Ultima modificacion : 12/08/2021

Otros cambios:
-Sin cambios nuevos
*/
var flag = false;
var files = [];
var filesEliminados = [];
var dropZoneLoadExistingFiles = false;
/**
 * configuración del componente de dropzone para el manejo de archivos
*/
var wrapperThis = null;
Dropzone.options.myDropzone = {
    url: "/",
    autoQueue: false,
    autoProcessQueue: false,
    uploadMultiple: true,
    parallelUploads: 100,
    maxFilesize: 2,
    maxFiles: 100,
    acceptedFiles: "image/*",
    dictDefaultMessage: "Arrastre los archivos aquí o haz click para subirlos",
      
    init: function () {
        wrapperThis = this;
      
        this.on("completemultiple", function() {
            console.log(this.getQueuedFiles());
        });
      
        this.on("addedfile", function (file) {

            flag = false;
            if (dropZoneLoadExistingFiles===false) {
                if(files.length > 0){

                    files.forEach(function(element) { 

                        if (file.name === element.name){
                            flag = true;
                        }
                    })
                    

                    if (flag == false){
                        console.log("3");
                        var reader = new FileReader();
                        reader.onload = function(event) {
                            // event.target.result contains base64 encoded image
                            var base64String = event.target.result;
                            var fileName = file.name;
                    
                            console.log(flag);
                            files.push({
                                dataURL: base64String,
                                name: fileName
                            });
                            flag = false;
                        };
                        reader.readAsDataURL(file);
                    }else{
                        console.log("4");
                        swal({
                            title: "Es posible que haya querido subir un archivo con el mismo nombre de los ya existentes, favor revisar.",
                            icon: "error",
                            buttons: {
                              confirm: {
                                text: "OK",
                                value: true,
                                visible: true,
                                className: "btn btn-dark",
                                closeModal: true,
                              },
                            },
                          });
                        wrapperThis.removeFile(file);
                    }                    
                    
                }else{
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        // event.target.result contains base64 encoded image
                        var base64String = event.target.result;
                        var fileName = file.name;
                
                        files.push({
                            dataURL: base64String,
                            name: fileName
                        });
                    };
                    reader.readAsDataURL(file);
                }
                
            }
            
            $("div.dz-size").remove();      
            // $("div.dz-details").remove();
            $("div.dz-progress").remove();
      
            var removeButton = Dropzone.createElement("<button class='btn btn-primary btn-cons'>Eliminar</button>");
      
            removeButton.addEventListener("click", function (e) {
                e.preventDefault();
                e.stopPropagation();      
                wrapperThis.removeFile(file);

                filesEliminados.push(file.id);

                $("div.dz-default.dz-message").hide();  
                if($("div.dropzone .dz-image-preview").length === 0) {
                  $("div.dz-default.dz-message").show();  
                }

                files = files.filter(function(value, index, arr){ 

                    if (value.urlArchivo === file.urlArchivo) {
                        return false;
                    }
                                        
                    if (value.name === file.name) {
                        return false;
                    }
                    console.log(files);
                    return true;
                });
            });
      
            file.previewElement.appendChild(removeButton);
        });
    }
};

//Se encarga de setear el dropzone con los archivos existentes
function SetFilesToDropZone(pFiles) {
    //console.log(pFiles);
    const newColumns = pFiles.map( item => {
        const { nombreArchivo: name, ...rest } = item;
        return { name, ...rest }
       }
    );
    dropZoneLoadExistingFiles = true;
    files = pFiles;
    var img = new Image();
    for (i = 0; i < newColumns.length; i++) {

        let extension = newColumns[i].name.split(".").pop();
        
        if(extension == "pdf"){
            wrapperThis.emit("addedfile", newColumns[i]);
            wrapperThis.emit("thumbnail", newColumns[i], '../../images/pdf.png');
            wrapperThis.emit("complete", newColumns[i]);
        }else{
            img.src =  newColumns[i].urlArchivo;
            wrapperThis.emit("addedfile", newColumns[i]);
            wrapperThis.emit("thumbnail", newColumns[i], img.src);
            wrapperThis.emit("complete", newColumns[i]);
        }  
    }
    $(".dropzone .dz-preview .dz-image img").css({"width": "120px", "height": "120px"});
    dropZoneLoadExistingFiles = false;
}
