/*
Titulo:  api-lista-productos.js
Descripción: Realiza los llamados al API de AWS

Datos del autor : 
-Asdrubal Torres Siles

Version 1.0
Ultima modificacion : 09/01/2022

Otros cambios:
-Documentacion y limpieza
*/

var AGRO = window.AGRO || {}; 
var apiClient = apigClientFactory.newClient(); //  Instancia del API de AWS

/**
 * @function loadViewProductos carga los data-set al JSgrid
 */
AGRO.getSolicitudes = async function (idUsuario) {

    let token = await useTokenV2();
    let result = await new Promise((resolve, reject)=>{ 
      return resolve(apiClient.solicitudesGetListaParaUsuarioPost(null, {id:idUsuario}, {headers: { Authorization: token }})); 
    });
    
      if (result.data.hasOwnProperty("errorMessage")) {
        ("use strict");
        swal({
          title: result.data.errorMessage,
          icon: "error",
          buttons: {
            confirm: {
              text: "OK",
              value: true,
              visible: true,
              className: "btn btn-dark",
              closeModal: true,
            },
          },
        });
        return;
      }
      
      if (result.status == 200) {
             return result.data;
      } else {
        ("use strict");
        swal({
          title: "Ha ocurrido un error al subir los archivos.",
          icon: "error",
          buttons: {
            confirm: {
              text: "OK",
              value: true,
              visible: true,
              className: "btn btn-dark",
              closeModal: true,
            },
          },
        });
        return;
      }
};

/**
   * @function compare Utilizando el metodo sort, ordena los elementos del array por nombre
   * @param a primer elemento
   * @param b segundo elemento
   * @return array ordenado por nombre.
*/
function compare(a, b) {

    let comparison = 0;
    if (a.nombre > b.nombre) {
        comparison = 1;
    } else if (a.nombre < b.nombre) {
        comparison = -1;
    }
    return comparison;
}
