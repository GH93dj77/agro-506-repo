
/*
Titulo:  add-usuario.js
Descripción: Gestiona funciones para crear-solicitud.html


Datos del autor : 
-Alexander Villegas Valverde

Version 1.0
Ultima modificacion : 20/03/2022

Otros cambios:
- z
*/

/**
 * @var $validator objeto jquery para la validacion campos del form
 */
var $validator;

/**
 * @var {JSON} loggedUserInfo informacion del usuario en la sesion actual 
 */
var loggedUserInfo;


var form;

/**
 * Se definen @$filesArray utilizado para guardar los url de la imágenes
 */
var filesArray = [];

  /**
     * Ejecuta configuraciones especificas de pagina dependiendo del rol. Esta funcion es llamada en el header de la pagina
     * @param {JSON} usuario JSON con informacion de usuario
  */
  function setPage(usuario) { 
    console.log(JSON.stringify)
    form = $('#wizard_with_validation').show();
      $.validator.addMethod("valueNotEquals", function(value, element, arg){
      return arg !== value;
    }, "Value must not equal arg.");

    loggedUserInfo = usuario;

      
    form.steps({
      headerTag: 'h3',
      bodyTag: 'fieldset',
      transitionEffect: 'slideLeft',
      onInit: function (event, currentIndex) {
          // $.AdminAlpino.input.activate();

          //Set tab width
          var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
          var tabCount = $tab.length;
          $tab.css('width', (100 / tabCount) + '%');

          //set button waves effect
          setButtonWavesEffect(event);
      },
      onStepChanging: function (event, currentIndex, newIndex) {
          if (currentIndex > newIndex) { return true; }

          if (currentIndex < newIndex) {
              form.find('.body:eq(' + newIndex + ') label.error').remove();
              form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
          }

          form.validate().settings.ignore = ':disabled,:hidden';
          return form.valid();
      },
      onStepChanged: function (event, currentIndex, priorIndex) {
          setButtonWavesEffect(event);
      },
      onFinishing: function (event, currentIndex) {
          form.validate().settings.ignore = ':disabled';
          return form.valid();
      },
      onFinished: function (event, currentIndex) {
         alert("finnnn");
         addUser();
      }
    });

    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            'nombre_': {
                required:true,
                minlength:4,
                maxlength:34,
            },
            'apellidos_': {
                required:true,
                minlength:4,
                maxlength:34,
            },
            'correo_': {
                required:true,
                minlength:4,
                maxlength:34,
                email:true
            },
            'tipo_': {
                required:true,
                minlength:1,
                valueNotEquals: ""
            }
            
        }
    });

    
Dropzone.options.myDropzone = {
  url: "/",
  autoQueue: false,
  autoProcessQueue: false,
  uploadMultiple: true,
  parallelUploads: 100,
  maxFilesize: 2,
  maxFiles: 100,
  acceptedFiles: "image/*,application/pdf",
  dictDefaultMessage: "Arrastre los archivos aquí o haz click para subirlos",
    
  init: function () {
      wrapperThis = this;
    
      this.on("completemultiple", function() {
          console.log(this.getQueuedFiles());
      });
    
      this.on("addedfile", function (file) {

          flag = false;
          if (dropZoneLoadExistingFiles===false) {
              if(files.length > 0){

                  files.forEach(function(element) { 

                      if (file.name === element.name){
                          flag = true;
                      }
                  })
                  

                  if (flag == false){
                      console.log("3");
                      var reader = new FileReader();
                      reader.onload = function(event) {
                          // event.target.result contains base64 encoded image
                          var base64String = event.target.result;
                          var fileName = file.name;
                  
                          console.log(flag);
                          files.push({
                              dataURL: base64String,
                              name: fileName
                          });
                          flag = false;
                      };
                      reader.readAsDataURL(file);
                  }else{
                      console.log("4");
                      swal({
                          title: "Es posible que haya querido subir un archivo con el mismo nombre de los ya existentes, favor revisar.",
                          icon: "error",
                          buttons: {
                            confirm: {
                              text: "OK",
                              value: true,
                              visible: true,
                              className: "btn btn-dark",
                              closeModal: true,
                            },
                          },
                        });
                      wrapperThis.removeFile(file);
                  }                    
                  
              }else{
                  var reader = new FileReader();
                  reader.onload = function(event) {
                      // event.target.result contains base64 encoded image
                      var base64String = event.target.result;
                      var fileName = file.name;
              
                      files.push({
                          dataURL: base64String,
                          name: fileName
                      });
                  };
                  reader.readAsDataURL(file);
              }
              
          }
          
          $("div.dz-size").remove();      
          // $("div.dz-details").remove();
          $("div.dz-progress").remove();
    
          var removeButton = Dropzone.createElement("<button class='btn btn-primary btn-cons'>Eliminar</button>");
    
          removeButton.addEventListener("click", function (e) {
              e.preventDefault();
              e.stopPropagation();      
              wrapperThis.removeFile(file);

              filesEliminados.push(file.id);

              $("div.dz-default.dz-message").hide();  
              if($("div.dropzone .dz-image-preview").length === 0) {
                $("div.dz-default.dz-message").show();  
              }

              files = files.filter(function(value, index, arr){ 

                  if (value.urlArchivo === file.urlArchivo) {
                      return false;
                  }
                                      
                  if (value.name === file.name) {
                      return false;
                  }
                  console.log(files);
                  return true;
              });
          });
    
          file.previewElement.appendChild(removeButton);
      });
  }
};

  
  }

// record.onclick = function() {
//   mediaRecorder.start();
//   console.log(mediaRecorder.state);
//   console.log("recorder started");
//   record.style.background = "red";
//   record.style.color = "black";
// }


  function disableRecordButton(disable){
    if(disable){
      $("#startRecordAudio_btn").addClass("disabled");
      $("#startRecordAudio_btn").prop('disabled', true);
    }else{
      $("#startRecordAudio_btn").removeClass("disabled");
      $("#startRecordAudio_btn").prop('disabled', false);
    }
    
  }


  
  function disableStopButton(disable){
    if(disable){
      $("#stopRecordAudio_btn").addClass("disabled");
      $("#stopRecordAudio_btn").prop('disabled', true);
    }else{
      $("#stopRecordAudio_btn").removeClass("disabled");
      $("#stopRecordAudio_btn").prop('disabled', false);
    }
    
  }


 async function audioRecorder(isRecording){
   if(isRecording){
      disableRecordButton(true);
      disableStopButton(false);
      // mediaRecorder.start();
      // console.log(mediaRecorder.state);
      // console.log("recorder started");
      // record.style.background = "red";
      // record.style.color = "black";
   }else{
      disableRecordButton(false);
      disableStopButton(true);
        // audio = mediaRecorder.stop();
        // console.log(mediaRecorder.state);
        // console.log("recorder stopped");
        // record.style.background = "";
        // record.style.color = "";
   }
 }


/**
  * @function checkForFiles se encarga de enviar los archivos a la API
*/
async function checkForFiles(){

  if(files.length > 0){

    for(let i = 0; i < files.length; i++){

      let extension_archivo = files[i].name.split('.');
      let params = {
        file: files[i].dataURL,
        fileExtension: extension_archivo[extension_archivo.length - 1],
        sourceLocation: "img-sol-archivos",
        fileName: files[i].name
      }
      let fileURL = await uploadFile(params);
      filesArray.push(fileURL)
    }
  }  
}


function addSolicitud(){
  let params ={
    nombre:$("#nombre_").val(),
    apellido:$("#apellidos_").val(),
    correo:$("#correo_").val(),
    tipo:$("#tipo_").val()
  }
  console.log("Params = " + JSON.stringify(params) );
  // AGRO.AddSolicitud(params);
}


function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}