/*
Titulo:  api-add-usuario.js
Descripción: Realiza los llamados al API de AWS

Datos del autor : 
-Alexander Villegas Valverde
-Empresa: TECHBITE

Version 1.0
Ultima modificacion : 02/03/2021

Otros cambios:
-Documentacion y limpieza
*/

var AGRO = window.AGRO || {};
var apiClient = apigClientFactory.newClient(); //  Instancia del API de AWS

/**
 * @function AddUser Añade un usuario a la base de datos Dynamo en AWS
 * @param params JSON con informacion del cliente
 */
AGRO.AddUser = function (params) {
  params.tipo = params.tipo.toString();
  $("#page-loader-wrapper").fadeIn(500);
  try {
    useToken(token, function (token) {
      apiClient
        .usuarioAddItemPost(null, params, {
          headers: {
            Authorization: token,
          },
        })
        .then(function (result) {
          console.log("RESULT = " + JSON.stringify(result));
          if (result.data.hasOwnProperty("errorMessage")) {
            alert(result.data.errorMessage);
            $("#page-loader-wrapper").fadeOut(500);
            return;
            // location.reload();
          }
          if (result.status == 200) {
            if (confirm("¿Desea agregar otro usuario?")) {
              location.reload();
            } else {
              window.location.href = "lista-usuarios.html";
            }

            // window.location.href = "view-fichatecnica.html"
          } else {
            $("#page-loader-wrapper").fadeOut(500);
            alert("Ocurrió un error al guardar los datos.");
            //  document.getElementById("loader-overlay-MOD").style.display = "none";
            return;
          }
        });
    });
  } catch (e) {
    console.log("error :" + e);
    $("#page-loader-wrapper").fadeOut(500);
    alert("Ocurrió un error con la conexión.");
  }
};



/**
 * @function uploadFile llamado del API para la subida de archivos al S3
*/
async function uploadFile(params, callback){

  console.log(JSON.stringify(params));
  let token = await useTokenV2();
  let result = await new Promise((resolve, reject)=>{ 
    return resolve(apiClient.contratosFileUploadPost(null, params, {headers: { Authorization: token }})); 
  });
  
    if (result.data.hasOwnProperty("errorMessage")) {
      ("use strict");
      swal({
        title: result.data.errorMessage,
        icon: "error",
        buttons: {
          confirm: {
            text: "OK",
            value: true,
            visible: true,
            className: "btn btn-dark",
            closeModal: true,
          },
        },
      });
      return;
    }
    
    if (result.status == 200) {
           return result.data;
    } else {
      ("use strict");
      swal({
        title: "Ha ocurrido un error al subir los archivos.",
        icon: "error",
        buttons: {
          confirm: {
            text: "OK",
            value: true,
            visible: true,
            className: "btn btn-dark",
            closeModal: true,
          },
        },
      });
      return;
    }
}

/**
 * @function compare Utilizando el metodo sort, ordena los elementos del array por nombre
 * @param a primer elemento
 * @param b segundo elemento
 * @return array ordenado por nombre.
 */
function compare(a, b) {
  let comparison = 0;
  if (a.nombre > b.nombre) {
    comparison = 1;
  } else if (a.nombre < b.nombre) {
    comparison = -1;
  }
  return comparison;
}
