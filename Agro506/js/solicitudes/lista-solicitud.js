/*
Titulo:  lista-solicitudes.js
Descripción: Gestiona funciones para lista-productos.html

Datos del autor : 
-Alexander Villega

Version 1.0
Ultima modificacion : 21/3/2021

Otros cambios:
Sin cambios nuevos
*/

/**
   * Ejecuta configuraciones especificas de pagina dependiendo del rol.
*/
async function setPage() {
   let solicitudes = await AGRO.getSolicitudes(globalUser.id);
   console.log(`solicitudes  = ${JSON.stringify(solicitudes)}`);
   loadData(solicitudes);
   $(".page-loader-wrapper").fadeOut(500);
}


function loadData(solicitudes){
  let html = [];
  solicitudes.forEach(function(sol){
    let estadoEscrito = sol.estado == 1? '<span class="badge badge-success">Revisada</span>':'<span class="badge badge-info">Pendiente</span>';
    let estado = sol.estado == 1? 'approved':'pending';
    let imagen = sol.url_fotos[0];
    console.log('imagen = ' + imagen);
    
    let aux = `
    <tr data-status="${estado}">
        <td><div class="media-object"><img src="${imagen}" alt="Not found" width="40px"></div></td>
        <td width="100">${sol.nombre}</td>
        <td>${sol.descripcion}</td>
        <td>${estadoEscrito}</td>
    </tr>
    `;
    html.push(aux);
  });
  $("#table_sol").append(html.join(''));
}

