
/*
Titulo:  add-producto.js
Descripción: Gestiona funciones para add-producto.html


Datos del autor : 
-Asdrubal Torres Siles

Version 1.0
Ultima modificacion : 

Otros cambios:
-
*/  
//Variables Globales
var filesArray= [];
var id_producto;
  
/**
  * @function load 
*/
$(window).on("load", function () {
  setPages();
});

/**
  * @function setPages se encarga de inicializar una serie de componentes
*/
function setPages() {

  id_producto = GetURLParameter("id");
  if (id_producto != undefined){
    
    getProductoDetalleAWS({ id_producto: id_producto });
  }  
}

/**
  * @event CrearProductoClick se encarga de registrar el producto
*/
$( "#btnCrearProducto" ).click(async function() {
  
  if (id_producto != undefined){
    EditarProducto();
  }else{
    GenerarProducto();
  }
});

/**
  * @function GenerarProducto se encarga de crear el producto
*/
async function GenerarProducto(){
  await checkForFiles();
  let contratoInfo = [];

  contratoInfo.push({
    nombre:$("#txt_nombreProducto").val(),
    fecha_vencimiento:$("#txt_fechaFin").val(),
    precio:$("#txt_Precio").val(),
    detalles:$("#ta_detalle").val()
  });

  let arrayFinal = {
    data: contratoInfo,
    filesURL: filesArray
  };

  console.log("Agregado");
  AgregarProducto(arrayFinal);
}

/**
  * @function EditarProducto se encarga de editar el producto
*/
async function EditarProducto(){

  var filtered = files.filter(function(el) { return el.dataURL != undefined; }); 
  files = filtered;
  await checkForFiles();
  $("#loader-overlay").fadeOut(800);
  let productoInfo = [];

  productoInfo.push({
    id_producto: id_producto,
    nombre:$("#txt_nombreProducto").val(),
    fecha_vencimiento:$("#txt_fechaFin").val(),
    precio:$("#txt_Precio").val(),
    detalles:$("#ta_detalle").val()
  });

  let arrayFinal = {
    data: productoInfo,
    filesURL: filesArray,
    filesEliminados: filesEliminados
  };

  EdicionProducto(arrayFinal);
}

/**
  * @function checkForFiles se encarga de enviar los archivos a la API
*/
async function checkForFiles(){

  if(files.length > 0){

    for(let i = 0; i < files.length; i++){

      let extension_archivo = files[i].name.split('.');
      let params = {
        file: files[i].dataURL,
        fileExtension: extension_archivo[extension_archivo.length - 1],
        sourceLocation: "productos",
        fileName: files[i].name
      }
      let fileURL = await uploadFile(params);
      console.log("Lleno el array de la URL");
      filesArray.push(fileURL)
    }
  }  
}

/**
   * @function GetURLParameter obtiene por url los parametros enviados
*/
function GetURLParameter(sParam) {
  var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split("&");
  for (var i = 0; i < sURLVariables.length; i++) {
    var sParameterName = sURLVariables[i].split("=");
    if (sParameterName[0] == sParam) {
      return sParameterName[1];
    }
  }
}

/**
   * @function loadInfo se encarga de leer toda la informacion recibida y imprimirla en pantalla
*/
function loadInfo(informacion) {

  $("#txt_nombreProducto").val(informacion.data.nombre);
  $("#txt_fechaFin").val(informacion.data.fecha_vencimiento);
  $("#txt_Precio").val(informacion.data.precio);
  $("#ta_detalle").val(informacion.data.detalles);
  console.log(informacion.data.archivos);
  SetFilesToDropZone(informacion.data.archivos);
}