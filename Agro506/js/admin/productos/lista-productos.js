/*
Titulo:  lista-productos.js
Descripción: Gestiona funciones para lista-productos.html

Datos del autor : 
-Asdrubal Torres Siles

Version 1.0
Ultima modificacion : 09/01/2022

Otros cambios:
Sin cambios nuevos
*/

/**
   * Ejecuta configuraciones especificas de pagina dependiendo del rol.
*/
function setPage() {
    AGRO.loadViewProductos();
    $(".page-loader-wrapper").fadeOut(500);
}