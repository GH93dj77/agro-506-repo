/*
Titulo:  api-lista-productos.js
Descripción: Realiza los llamados al API de AWS

Datos del autor : 
-Asdrubal Torres Siles

Version 1.0
Ultima modificacion : 09/01/2022

Otros cambios:
-Documentacion y limpieza
*/

var AGRO = window.AGRO || {}; 
var apiClient = apigClientFactory.newClient(); //  Instancia del API de AWS

/**
 * @function loadViewProductos carga los data-set al JSgrid
 */
AGRO.loadViewProductos = function (currentUserID) {
    useToken(token, function (token) {
        apiClient.productosListaGet({}, null, {
                headers: {
                    Authorization: token
                }
            })
            .then(function (result) {
                if (typeof result.data !== 'undefined' && result.data.length <= 0) {
                    $("#jsgrid").jsGrid({
                        noDataContent: "No se encontraron registros"
                    });
                    $("#jsgrid").jsGrid("option", "data", []);
                    return;
                }
                var client = result.data;
                $("#jsgrid").jsGrid({
                    controller: {
                        loadData: function (filter) {
                            return $.grep(client, function (client) {
                                return (!filter["nombre"].toLowerCase() || client["nombre"].toLowerCase().indexOf(filter["nombre"].toLowerCase()) > -1) &&
                                    (!filter["precio"].toLowerCase() || client["precio"].toLowerCase().indexOf(filter["precio"].toLowerCase()) > -1) &&
                                    (!filter["fecha_vencimiento"].toLowerCase() || client["fecha_vencimiento"].toLowerCase().indexOf(filter["fecha_vencimiento"].toLowerCase()) > -1) &&
                                    (!filter["detalles"] || client["detalles"].indexOf(filter["detalles"]) > -1)
                                    
                            });
                        },
                        onItemUpdating: function (args) {
                            console.log("UPDATE!!");
                            // cancel update of the item with empty 'name' field

                        },
                        updateItem: function (item) {
                        }

                    },

                    noDataContent: "No se encontraron productos",
                    sorting: true,
                    paging: true,

                    filtering: true,
                    data: client
                });
                $("#jsgrid").jsGrid("loadData");
            });

    });
};

/**
 * @function loadViewProductos carga los data-set al JSgrid
 */
 AGRO.EliminarProducto = function (params) {

    try {
        useToken(token, function (token) {
            apiClient.productosEliminarDelete(null, params, {
                headers: {
                    Authorization: token
                }
            })
            .then(function (result) {
                if (result.data.hasOwnProperty("errorMessage")) {
                    swal("Error!", "Ya existe un producto con ese código.", "error");
                    return;
                }
                if (result.status == 200) {
                    swal({
                        title: "Éxito!",
                        text: "El producto ha sido eliminado exitosamente!",
                        type: "success",
                        confirmButtonColor: "#8CD4F5",
                        confirmButtonText: "Aceptar!",
                        closeOnConfirm: false
                    }, function () {
                        location.reload();
                    });
                } else {
                    swal("Error!", "Ocurrió un error al guardar los datos.", "error");
                    return;
                }
            });
        });        

    } catch (e) {
        swal("Error!", "Ocurrió un error con la conexión.", "error");
    }
};

/**
   * @function compare Utilizando el metodo sort, ordena los elementos del array por nombre
   * @param a primer elemento
   * @param b segundo elemento
   * @return array ordenado por nombre.
*/
function compare(a, b) {

    let comparison = 0;
    if (a.nombre > b.nombre) {
        comparison = 1;
    } else if (a.nombre < b.nombre) {
        comparison = -1;
    }
    return comparison;
}
