/*
Titulo:  api-add-producto.js
Descripción: Realiza los llamados al API de AWS

Datos del autor : 
-Asdrubal Torres Siles

Version 1.0
Ultima modificacion : 19/01/2022

Otros cambios:
*/

var AGRO = window.AGRO || {};
var apiClient = apigClientFactory.newClient(); //  Instancia del API de AWS

/**
 * @function uploadFile llamado del API para la subida de archivos al S3
*/
async function uploadFile(params, callback){

  let token = await useTokenV2();
  console.log(token);
  let result = await new Promise((resolve, reject)=>{ 
    return resolve(apiClient.productosFileUploadPost(null, params, {headers: { Authorization: token }})); 
  });
  
    if (result.data.hasOwnProperty("errorMessage")) {
      ("use strict");
      swal({
        title: result.data.errorMessage,
        icon: "error",
        buttons: {
          confirm: {
            text: "OK",
            value: true,
            visible: true,
            className: "btn btn-dark",
            closeModal: true,
          },
        },
      });
      return;
    }
    
    if (result.status == 200) {
           return result.data;
    } else {
      ("use strict");
      swal({
        title: "Ha ocurrido un error al subir los archivos.",
        icon: "error",
        buttons: {
          confirm: {
            text: "OK",
            value: true,
            visible: true,
            className: "btn btn-dark",
            closeModal: true,
          },
        },
      });
      return;
    }
}

/**
 * @function AgregarProducto llamado del API para la creación del producto
*/
function AgregarProducto(params) {

  useToken(token, function (token) {
    apiClient
      .productosAgregarPost(null, params, {
        headers: { Authorization: token },
      })
      .then(function (result) {
        $("#loader-overlay").fadeOut(800);
        if (result.data.hasOwnProperty("errorMessage")) {
          alert("Error!");
        }

        if (result.status == 200) {
            alert("Guardado Exitoso!");
            window.location.href = "lista-productos.html";
        } else {
          alert("Error!");
        }
      });
  });
}

/**
   * @function getProductoDetalleAWS se encarga de obtener la informacion de un producto en especifico
*/
function getProductoDetalleAWS(params) {    
  useToken(token, function (token) {
    apiClient
      .productosObtenerDetallePost(null, params, {
        headers: {
          Authorization: token,
        },
      })
      .then(function (result) {
        if (result.data != null && result.data.errorMessage) {
          if (result.data.errorMessage) {
            _handleErrorMessage(
              result.data.errorMessage,
              "lista-productos.html",
              true
            );
          }
          return;
        }
          loadInfo(result);
      });
  });
}

/**
 * @function EdicionProducto llamado del API para la edición del contrato
*/
function EdicionProducto(params) {

  useToken(token, function (token) {
    apiClient
      .productosEditarPost(null, params, {
        headers: { Authorization: token },
      })
      .then(function (result) {
        $("#loader-overlay").fadeOut(800);
        if (result.data.hasOwnProperty("errorMessage")) {
          ("use strict");
          alert("Error!");
        }

        if (result.status == 200) {
            alert("Guardado Exitoso!");
            window.location.href = "lista-productos.html";
        } else {
          ("use strict");
          alert("Error!");
          $("#loader-overlay").fadeOut(800);
          return;
        }
      });
  });
}