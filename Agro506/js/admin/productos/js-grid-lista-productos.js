/*
Titulo:  js-grid-lista-productos.js
Descripción: Gestiona funciones del JS-grid perteneciente a lista-productos.html

Datos del autor : 
-Asdrubal Torres Siles

Version 1.0
Ultima modificacion : 09/01/2022

Otros cambios:
Sin cambios nuevos
*/

var apiClient = apigClientFactory.newClient(); // Instancia del API de aws

(function ($) {
  "use strict";
  $("#sort").click(function () {
    var field = $("#sortingField").val();
    $("#sorting-table").jsGrid("sort", field);
  });

  $(function () {
    $("#jsgrid").jsGrid({
      width: "100%",
      autoload: true,
      pageIndex: 1,
      pageSize: 30,
      pageButtonCount: 15,
      pagerFormat: "{prev} {pages} {next} ",
      paging: true,
      noDataContent: "Cargando...",
      pagePrevText: "<",
      pageNextText: ">",
      sorting: true,
      editing: false,
      filtering: true,
      deleteConfirm: "¿Seguro que desea eliminar este registro?",
      /**
       * Definicion de propiedades de JS-grid
       */
      fields: [
        {
          name: "nombre",
          title: "Nombre",
          type: "text",
          width: 100,
          css: "text-truncate text-center",
          editing: false,
        },
        {
          name: "precio",
          title: "Precio",
          type: "text",
          width: 100,
          css: "text-truncate text-center",
          editing: false,
        },
        {
          name: "detalles",
          title: "Detalles",
          type: "text",
          width: 100,
          css: "text-truncate text-center",
          editing: false,
        },        
        {
          name: "fecha_vencimiento",
          title: "Fecha vencimiento",
          type: "date",
          width: 100,
          css: "text-truncate text-center",
          editing: false,
        },
        {
          name: "Editar",
          title: "Editar",
          width: 50,
          css: "text-truncate text-center",
          sorting: false,
          itemTemplate: function (value, item) {
            return $("<button>")
              .addClass("btn btn-primary btn-icon")
              .append($("<i>").addClass("zmdi zmdi-edit"))
              .on("click", function () {
                window.location.href = "add-producto.html?id=" + item.id_producto;
              });
          },
        },
        {
          name: "Eliminar",
          title: "Eliminar",
          width: 60,
          sorting: false,
          itemTemplate: function (value, item) {
            // deshabilitación de usuario
            let disabled = item.estado == "0" ? "disabled" : "";
            return $("<button>")
              .addClass("btn btn-danger btn-icon " + disabled + " ")
              .append($("<i>").addClass("zmdi zmdi-delete"))
              .append($("<i>").addClass("fa fa-ban"))
              .on("click", function () {

                swal({
                  title: "¿Está seguro?",
                  text: "No podrás volver a recuperar el registro!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Si, bórralo!",
                  cancelButtonText: "No, cancélalo!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      let param = {
                        id_producto: item.id_producto
                      }
                      AGRO.EliminarProducto(param);
                      //location.reload();
                    } else {
                      swal("Cancelado", "Tu registro está a salvo :)", "error");
                    }
                });
              });
          },
        },
      ],
    });
  });
})(jQuery);
