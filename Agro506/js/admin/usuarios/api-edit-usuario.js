/*
Titulo:  api-add-usuario.js
Descripción: Realiza los llamados al API de AWS

Datos del autor : 
-Alexander Villegas Valverde
-Empresa: TECHBITE

Version 1.0
Ultima modificacion : 02/03/2021

Otros cambios:
-Documentacion y limpieza
*/

var AGRO = window.AGRO || {};
var apiClient = apigClientFactory.newClient(); //  Instancia del API de AWS

/**
 * @function editUser edita un usuario en base de datos Dynamo en AWS
 * @param params JSON con informacion del cliente
 */
AGRO.editUser = function (params) {
  console.log("==> " + JSON.stringify(params));
  alert();
  params.tipo = params.tipo.toString();
  $("#page-loader-wrapper").fadeIn(500);
  try {
    useToken(token, function (token) {
      apiClient
        .usuarioEditItemPost(null, params, {
          headers: {
            Authorization: token,
          },
        })
        .then(function (result) {
          console.log("RESULT = " + JSON.stringify(result));
          if (result.data.hasOwnProperty("errorMessage")) {
            alert(result.data.errorMessage);
            $("#page-loader-wrapper").fadeOut(500);
            return;
            // location.reload();
          }
          
          if (result.status == 200) {
            window.location.href = "lista-usuarios.html";
            // window.location.href = "view-fichatecnica.html"
          } else {
            $("#page-loader-wrapper").fadeOut(500);
            alert("Ocurrió un error al guardar los datos.");
            //  document.getElementById("loader-overlay-MOD").style.display = "none";
            return;
          }
        });
    });
  } catch (e) {
    console.log("error :" + e);
    $("#page-loader-wrapper").fadeOut(500);
    alert("Ocurrió un error con la conexión.");
  }
};


/**
   * @function getUsuario Obtiene los datos de un solo usuario
   * @param params JSON con id del usuario
*/
AGRO.getUsuario = function (params,callback) {
  try {
      useToken(token, function (token) {
          apiClient.usuarioGetItemPost(null, params, {
                  headers: {
                      Authorization: token
                  }
              })
              .then(function (result) {
                  
                  if (result.data.hasOwnProperty("errorMessage")) {
                      alert("Error: " + result.data.errorMessage);
                      document.getElementById("loader-overlay-MOD").style.display = "none";
                      window.location.href = "lista-infografico.html";
                      return
                      // location.reload();
                  }
                  if (result.status == 200) {
                      callback(result.data);
                  } else {
                      document.getElementById("loader-overlay-MOD").style.display = "none";
                      alert("Ocurrió un error al guardar los datos.");
                      //  document.getElementById("loader-overlay-MOD").style.display = "none";
                      return;
                  }
              });


      });

  } catch (e) {

      console.log("error :" + e);
      document.getElementById("loader-overlay-MOD").style.display = "none";
      alert("Ocurrió un error con la conexión.")

  }

}



/**
 * @function compare Utilizando el metodo sort, ordena los elementos del array por nombre
 * @param a primer elemento
 * @param b segundo elemento
 * @return array ordenado por nombre.
 */
function compare(a, b) {
  let comparison = 0;
  if (a.nombre > b.nombre) {
    comparison = 1;
  } else if (a.nombre < b.nombre) {
    comparison = -1;
  }
  return comparison;
}
