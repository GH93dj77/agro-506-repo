
/*
Titulo:  add-usuario.js
Descripción: Gestiona funciones para add-usuario.html


Datos del autor : 
-Alexander Villegas Valverde

Version 1.0
Ultima modificacion : 8/1/2021

Otros cambios:
-
*/

/**
 * @var $validator objeto jquery para la validacion campos del form
 */
 var $validator;

 /**
  * @var {JSON} loggedUserInfo informacion del usuario en la sesion actual 
  */
 var loggedUserInfo;
 
 /**
  * Se definen @$validator con la reglas del form
  */
 var form;
 var idUser; 
     
 
  //    $validator = $("#frm-add").validate({
  //        rules: {
  //            nombreField: {
  //            required: true,
  //            minlength: 1
  //          },
  //          apellidosField: {
  //            required: true,
  //            minlength: 1
  //          },
  //          correoField: {
  //            required: true,
  //            minlength: 1
  //          }
  //        }
  //      });
 
  //    $(".form-temas").first().append(new Option("option text1", "value1"));
  //  });
 
 
 
   
   
   /**
      * Ejecuta configuraciones especificas de pagina dependiendo del rol. Esta funcion es llamada en el header de la pagina
      * @param {JSON} usuario JSON con informacion de usuario
   */
   function setPage(usuario) { 
     form = $('#wizard_with_validation').show();
       $.validator.addMethod("valueNotEquals", function(value, element, arg){
       return arg !== value;
     }, "Value must not equal arg.");

     loggedUserInfo = usuario;
     if (GetURLParameter("") != "") {
      idUser = GetURLParameter("");
      AGRO.getUsuario( {id:idUser} ,setEdit);
    } else {
      window.location.href = "lista-usuarios.html";
    }
 


    /*
       var rol = usuario.tipo.N.toString();
       switch (rol) {
           case "1":
               // encaso de tipo 1
               break;
           case "2":
               window.location.href = "../home/index.html";
               break;
           case "3":
               window.location.href = "../home/index.html";
               break;
       }
   */
       // setTimeout(function () {
       //     $('.loader-wrapper').fadeOut('slow', function () {
       //         $(this).remove();
       //     });
       // }, 300);
   
   }


  function setEdit(usuario){
       
    form.steps({
      headerTag: 'h3',
      bodyTag: 'fieldset',
      transitionEffect: 'slideLeft',
      onInit: function (event, currentIndex) {
          // $.AdminAlpino.input.activate();

          //Set tab width
          var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
          var tabCount = $tab.length;
          $tab.css('width', (100 / tabCount) + '%');

          //set button waves effect
          setButtonWavesEffect(event);
      },
      onStepChanging: function (event, currentIndex, newIndex) {
          if (currentIndex > newIndex) { return true; }

          if (currentIndex < newIndex) {
              form.find('.body:eq(' + newIndex + ') label.error').remove();
              form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
          }

          form.validate().settings.ignore = ':disabled,:hidden';
          return form.valid();
      },
      onStepChanged: function (event, currentIndex, priorIndex) {
          setButtonWavesEffect(event);
      },
      onFinishing: function (event, currentIndex) {
          form.validate().settings.ignore = ':disabled';
          return form.valid();
      },
      onFinished: function (event, currentIndex) {
         addUser();
      }
    });

    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            'nombre_': {
                required:true,
                minlength:4,
                maxlength:34,
            },
            'apellidos_': {
                required:true,
                minlength:4,
                maxlength:34,
            },
            'correo_': {
                required:true,
                minlength:4,
                maxlength:34,
                email:true
            },
            'tipo_': {
                required:true,
                minlength:1,
                valueNotEquals: ""
            }
            
        }
    });
    console.log("usuarios = " + JSON.stringify(usuario));
    $("#nombre_").val(usuario.nombre);
    $("#apellidos_").val(usuario.apellido);
    $("#correo_").val(usuario.correo);
    $("#tipo_").val(usuario.tipo);
  }
 
 function addUser(){
  $("#page-loader-wrapper").fadeIn(500);
    let params = 
    {
      id:idUser,
      correo:$("#correo_").val(),
      nombre:$("#nombre_").val(),
      apellidos:$("#apellidos_").val(),
      tipo:$("#tipo_").val(),
      urlFoto:""
    };
    AGRO.editUser(params);
  }
 
 /**
  * Reliza una validacion en el form y si resulta @true realiza el llamado al api para registrar al usuario
  */
 $("#btn-save").click(function(e)
 {
   var $valid = $("#frm-add").valid();
   if ($valid) {
 
     
   }else
   {
     $validator.focusInvalid();
   }
 });
 
 function setButtonWavesEffect(event) {
     $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
     $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
 }

 /**
 * @function GetURLParameter obtiene el valor del parametro GET en el url
 * @param {string} sParam parametro
 */
function GetURLParameter(sParam) {
  var searchString = "";
  var sPageURL = window.location.search.substring(1);
  var hPageURL = window.location.hash.substring(1);

  if (sPageURL != "") {
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
      var sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] == sParam) {
      searchString = sParameterName[1];
    }
  } else if (hPageURL != "") {
    searchString = window.location.hash.substring(1);
  }
  return searchString;
}

