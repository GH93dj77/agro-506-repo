
/*
Titulo:  add-usuario.js
Descripción: Gestiona funciones para add-usuario.html


Datos del autor : 
-Alexander Villegas Valverde

Version 1.0
Ultima modificacion : 8/1/2021

Otros cambios:
-
*/

/**
 * @var $validator objeto jquery para la validacion campos del form
 */
var $validator;

/**
 * @var {JSON} loggedUserInfo informacion del usuario en la sesion actual 
 */
var loggedUserInfo;

/**
 * Se definen @$validator con la reglas del form
 */
var form;

    
var photoFile;
  //   $validator = $("#frm-add").validate({
  //       rules: {
  //           nombreField: {
  //           required: true,
  //           minlength: 1
  //         },
  //         apellidosField: {
  //           required: true,
  //           minlength: 1
  //         },
  //         correoField: {
  //           required: true,
  //           minlength: 1
  //         }
  //       }
  //     });

  //   $(".form-temas").first().append(new Option("option text1", "value1"));
  // });



  
  
  /**
     * Ejecuta configuraciones especificas de pagina dependiendo del rol. Esta funcion es llamada en el header de la pagina
     * @param {JSON} usuario JSON con informacion de usuario
  */
  function setPage(usuario) { 
    
    form = $('#wizard_with_validation').show();
      $.validator.addMethod("valueNotEquals", function(value, element, arg){
      return arg !== value;
    }, "Value must not equal arg.");

    loggedUserInfo = usuario;
  
      /*
      var rol = usuario.tipo.N.toString();
      switch (rol) {
          case "1":
              // encaso de tipo 1
              break;
          case "2":
              window.location.href = "../home/index.html";
              break;
          case "3":
              window.location.href = "../home/index.html";
              break;
      }
  */
      // setTimeout(function () {
      //     $('.loader-wrapper').fadeOut('slow', function () {
      //         $(this).remove();
      //     });
      // }, 300);

      
    form.steps({
      headerTag: 'h3',
      bodyTag: 'fieldset',
      transitionEffect: 'slideLeft',
      onInit: function (event, currentIndex) {
          // $.AdminAlpino.input.activate();

          //Set tab width
          var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
          var tabCount = $tab.length;
          $tab.css('width', (100 / tabCount) + '%');

          //set button waves effect
          setButtonWavesEffect(event);
      },
      onStepChanging: function (event, currentIndex, newIndex) {
          if (currentIndex > newIndex) { return true; }

          if (currentIndex < newIndex) {
              form.find('.body:eq(' + newIndex + ') label.error').remove();
              form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
          }

          form.validate().settings.ignore = ':disabled,:hidden';
          return form.valid();
      },
      onStepChanged: function (event, currentIndex, priorIndex) {
          setButtonWavesEffect(event);
      },
      onFinishing: function (event, currentIndex) {
          form.validate().settings.ignore = ':disabled';
          return form.valid();
      },
      onFinished: function (event, currentIndex) {
         addUser();
      }
    });

    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            'nombre_': {
                required:true,
                minlength:4,
                maxlength:34,
            },
            'apellidos_': {
                required:true,
                minlength:4,
                maxlength:34,
            },
            'correo_': {
                required:true,
                minlength:4,
                maxlength:34,
                email:true
            },
            'tipo_': {
                required:true,
                minlength:1,
                valueNotEquals: ""
            }
            
        }
    });
  
  }

function addUser(){
  let params ={
    nombre:$("#nombre_").val(),
    apellido:$("#apellidos_").val(),
    correo:$("#correo_").val(),
    tipo:$("#tipo_").val()
  }
  AGRO.AddUser(params);
}

/**
 * Reliza una validacion en el form y si resulta @true realiza el llamado al api para registrar al usuario
 */
$("#btn-save").click(async function(e)
{
  var $valid = $("#frm-add").valid();
  if ($valid) {

    // if(document.getElementById('file-cvs').files.length > 0 )
    // {
    //   // document.getElementById("loader-overlay-MOD").style.display = "block";   
    //   //$("#loader-overlay-MOD").addClass("d-flex");   
    //   photoFile = document.getElementById('file-prof').files[0];

    //   let extension_archivo = photoFile[i].name.split('.');
    //   let params = {
    //     file: photoFile[i].dataURL,
    //     fileExtension: extension_archivo[extension_archivo.length - 1],
    //     sourceLocation: "productos",
    //     fileName: photoFile[i].name
    //   }

    //   let fileURL = await uploadFile(params);

    // }else
    // {
    //   alert("Debe de subir un archivo antes de continuar");
    //   return;
    // }

    document.getElementById("loader-overlay-MOD").style.display = "block";
    let userparams = 
    {
      correo:$("#correoField").val(),
      nombre:$("#nombreField").val(),
      apellidos:$("#apellidosField").val(),
      tipo:$("#seleTipo").val(),
      urlFoto:"../../assets/img/profiles/bb.jpg"
    };
    JSON.stringify(userparams);
   // AGRO.AddUser(params)
  }else
  {
    $validator.focusInvalid();
  }
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}


/**
  * @function checkForFiles se encarga de enviar los archivos a la API
*/
async function checkForFiles(){

  if(files.length > 0){

    for(let i = 0; i < files.length; i++){

      let extension_archivo = files[i].name.split('.');
      let params = {
        file: files[i].dataURL,
        fileExtension: extension_archivo[extension_archivo.length - 1],
        sourceLocation: "productos",
        fileName: files[i].name
      }
      let fileURL = await uploadFile(params);
      console.log("Lleno el array de la URL");
      filesArray.push(fileURL)
    }
  }  
}